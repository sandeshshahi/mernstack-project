
// mutable behaviour
// immutable behaviour

var address = 'tinkune kathmandu';  // orignal


var address1 = address ; //reference

// address='lalitpur';
address1= 'bkt';

console.log('address is>>',address);
console.log('address 1 is>>',address1);

//immutable behaviour -- > if changed in original it is not reflected in reference and viceversa


//##############################################################

var student = {  //original
    name: 'nilaww',
    adddress:'ktm',
    phone: 23232323,
    email:'abc@gmail.com'

}

var student1 =student;// reference
// student.email='xyz@gmail.com';
student1.status='online';

console.log('student >>',student)
console.log('student 1 is>>',student1)

// if changed in original it is reflected in reference
// if changed in reference it is reflected in original
// mutable behaviour


function sendMail(args){
    args.status = 'seen'
}

var mailData = {
    from:'abc@gmail.com',
    to:'test@gmail.com',
    sub:'forget password',
    message:'welcome to somewhere'

}

console.log('mail data before function call',mailData);
sendMail(mailData);

console.log('mail data after function call>>',mailData)


// *** array ra object ma kaam garda mutable behaviour dekhincha!!
