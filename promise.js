// function askMoney(amt,a,b) {

// }

// askMoney(function(){


// })

// function askMoney(amt,cb){
//     console.log('i need to wait');
//     setTimeout (function () {
//         cb(null,'asbds')
//     }, 2000)
// }

// askMoney(333,function(err,done){
//     if(err){
//         console.log('error in ask money',err)
//     }else{
//         console.log('success in ask money >>',done)
        
//     }
// })

// function buyPhone(model,errcb,successcb){
//     // logic
//     // errcb();
//     successcb();
// }

// buyPhone('s10',function(err){
//     console.log('error in buy phone',err);

// },function(data){
//     console.log('success in buy phone',data);

// })


// truthy value ==> any defined value when checking condition JS will not look for boolean
// falsy value ==> null,undefined,false,0,'',NaN


// ###################################################################################################################

// object is collection of key value pair
// var duster = {
//     size:'small',
//     price:123,
//     color:'black',
//     getPrice: function () {


//     } ,
//     tags:['duster','stationary'],
//     more_details:{
//         mauDate:'something',
//         distributor:'asfdf'
//     }

// }

// if object's property has value as a function we call that property as method
// for value oother then function is property 

// PROMISE
// promise is an object which holds future result
// promise is used to handle result of async call

// promise has three different state

//(// promise bhanya object object bhitra key value ra tyo value ma function cha bhane method bhanincha)

// promise has four different methods  
// 1. pending ===> promise is initialized but result is not ready
// 2. unfullfilled ===> result is ready with success
// 3. unRejection ===> result is ready with failure
// 4. settled ====> once we have result (either failure or success) it is settled

// IMP ===> Once promise is settled it will never change its state
// promise can go to fullfilled or rejection from pending once state is changed from pending it will never change its state

// promise has three different methods
// then ===> then method is used to handle success and failure both
    // reccomendation to use then to handle success only
// catch ===> catch method is used to handle failure only
// finally ===> once promise is settled this method will execute


// syntax

// new is used to call constructor

// var abc = new Promise(function(success,failure){
// success or 1st argument is for success callback
// failure or 2nd argument is for failure callback
// })

// console.log('abc is >>', abc);

function askMoney(amt){
    var xyz = new Promise(function( resolve , reject){
        console.log('i asked money with mom')
        setTimeout(function(){
            console.log('mom got salary');
            //  resolve(amt-222)//success
            // resolve('money')//success
            // reject('no money')///failure
            reject('again')///failure


        },1000)

    })
    return xyz;
}


var a = askMoney(44);
// console.log('a is ===>',a);
a
    .then(function(data){
    console.log('inside then success >>', data)
    // console.log(sandesh)
 }//, function (err) {
//     console.log('inside failure of then >>',err)} // yesari ni garna sakiyo or use catch 
)
    .catch(function(err){
       console.log('inside catch block >>',err) 
    })

    .finally(function(data){
        console.log('promise is settled',data)
    })

// callback,promise duitai ko kaam result handle garne ho
// callback le chai state maintain gardaina
// promise le state maintain garcha
