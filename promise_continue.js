// ##################################### TASK PART #############################################
function askMoney(amt){
    return new Promise(function(resolve, reject){
        console.log('mom told me to wait')
            
        setTimeout(function(){
            console.log('mom receives her salary')
            resolve(amt+10000); 
        },1000)
        
    })
}

function buyPhone(model){
    return new Promise(function(resolve,reject){

        console.log('model is not available right now so i should wait')
        setTimeout(function(){
            console.log('model found at shop')
            resolve(model)
        },1000)
    })
}

function takePhoto(location){
    return new Promise(function(resolve,reject){

        console.log('choosing loacation to take photo')
        setTimeout(function(){
            console.log('location found and photo taken')
            resolve('done');
        },1000)
    })
}

function editPhoto(rawPhoto){
    return new Promise(function(resolve,reject){

        console.log('editing in progress')
        setTimeout(function(){
            console.log('editing done')
            resolve('completed')
        }, 1000)
    })
}

function upLoadPhoto(editedPhoto){
    return new Promise(function(resolve,reject){

        console.log('upload in progress');
        setTimeout(function(){
            console.log('upload completed');
            resolve('done')
        },1000)
    })
}

// #################################### TASK PART #############################################



// *********************************** EXECUTION PART *****************************************

console.log('start execution');

askMoney(2356)
    .then(function(data){
        console.log('success part of promise',data)
        console.log('now buy phone')
        var promiseOfBuyPhone =  buyPhone('12promax')
        console.log('have coffee'); // non blocking code ko lagi yesari garnu parcha promise ma
        return promiseOfBuyPhone;
    })
    .then(function(data){
        console.log('result of buy phone>>',data)
        console.log('takephoto');
        return takePhoto('dhulikhel')
    })
    .then(function(data){
        console.log('result of take photo',data);
        console.log('now edit photo');
        return editPhoto('asdasdsa.pnj')
     })
    .then(function(data){
        console.log('success of edit photo')
        console.log('now upload photo');
        return upLoadPhoto('asd');

    })
    .then(function(data){
        console.log('success of upload photo',data);
    })

    .catch(function(err){
        console.log('inside catch block',err)
    })


    console.log('eat food')
    



// *********************************** EXECUTION PART *****************************************
