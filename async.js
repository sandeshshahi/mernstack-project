// synchronous 
// asynchronous

// synchronous

// code executes in a wway step by step
// step1
// step2 result waiting (ie step 2 waiting for result of step 1)
// step3  (step 2 ko result ready hunu paryo)
// step4  ( step 3 ko result ready hunu paryo)

// step n ===> step n-1 ko result ready hunu paryo

// eg
// db_connect();
// data_validation();
// db_insert();
// send_notification();


// advantages
// easy to implement logic
// readable codebase

// disadvantages
// slow execution ( non blocking task)



// asynchronous

// step1
// step2 (non result waiting)
// step3
// step4

// advantages
// fast execution (for non blocking code)

// disadvantage
// difficulty in implementing logic
// slightly unreadble codebase


// step n-1 ko result parkhinu pardaina for step n

// eg
// data_validation();
// db_connect();
// db_insert();
// send_notification();

//biggest challenges in asynchronous execution behaviour
// result handling

// JS lai asynchronous manna sakincha cause it event driven program diagphm


console.log('step 1')
console.log('step 2')
console.log('step 3')
// delay function
setTimeout(function(){
    
    console.log('step 4')

}, 1000);

console.log('step 5')
console.log('step 6')

