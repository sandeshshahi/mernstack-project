// closure 
// closure is an inner ffunction which has access to
// 1. global scope
// 2. parent function scope an argument
// 3. own scope and argument

var text = 'welcome';
function welcome(name){
    var to = 'to';

    function inner(place){
        var greetingtext = 'hi ' +name+', '+text+' '+to+' '+place
        console.log('greeting text >>',greetingtext)
        return 'hello';
    }
    // inner('ktm')
    // welcome.inner=inner; or 
    return inner;

}
var res = welcome('sandesh')('pokhara')
console.log('res >>',res);


// welcome('sandesh')
// welcome.inner('jhapa')   

// task is to
// create a function to multiply

// var res = multiply (3)(4)
// closure

function multiply(num1){
    function inner1(num2){
        function inner2(num3){
        var result = num1 * num2 * num3;
    
        return result;
}
    return inner2;

    }

    return inner1;

}

var result = multiply(3)(4)(2);
console.log('result >>',result)


// challenges 
// var re= multiply (2)(3)(4)
