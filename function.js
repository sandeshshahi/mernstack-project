// function ==> function is a resuable block of code that performs specific task

// syntax 
// function(){

// }

// two wways of writing a function
// 1. expression syntax
// 2. declaration syntax

// 1. expression syntax
// var welcome;
// console.log('check welcome ..',welcome)
// welcome();
//  var welcome = function(){   //variable declaration
    
//     console.log('i am welcome function')



//  }

//  welcome();

 

// 2. declaration syntax 
// hello();
    
// function hello(){  //funtion declaration
//         console.log('i am hello function')

// }

// hello();


// hoisting ((important question in interview))
// hoising is a mechanishmm that moves all the declaration at top before execution 


// types of function

// 1. named function
// 2. unnamed function (anynomous function)
// 3. funtion with argument
// 4. function with return type
// 5. IIFE (Immidiately Invoked Functional Expression)

// 1. named funtion
// welcome and hello are named function


// 2. anynomous function
// function without name is anynomous function
// function(){


// }

// 3. funtion with argument

// function sayHello(name){ //tei pass gareko value ie argument function maa pugepacchi ((parameters)) bhanincha
//     // what comes in
//     // console.log('what comes in >>', name)
//     var text='hi '+name+' , welcome to MERN';
//     console.log('text is >>', text)
// }

// sayHello('sandesh'); // sandesh pass hune value ho jaslai ((argument)) bhanincha
// sayHello('ram');// sandesh pass hune value ho jaslai ((argument)) bhanincha
// sayHello('hari'); // sandesh pass hune value ho jaslai ((argument)) bhanincha


// function with multiple argument

// function mulaarg(name,address){
    // var 
// }

// mulaarg('sandesh','baneshwor');
// mulaarg('hari','koteshwor');
// multiple argument ma dhyan dine order of argument passedd

// recommendation
// if we have more than 3 arguments reduce it to single arguments

// function sendMail(from,to,sub,message,replyTo){
// TODO prepare mail

// }

// // sendMail('abc@gmail.com','b',null,null,'acdl.com')

// function sendMail(a){
// // mail content 
// // a.from
// // a.to
// // a.subject


// }

// var mailData ={
//     from:'abc@gmail.com',
//     to:'gg@gmail.com',
//     sub:'forget password',
//     text:'hi',
//     message:'hello',
//     a:'n',
//     c:'d',
//     xyz:'abc'

// }

// sendMail(mailData);


// 4. function with return type


// function multiply(num1,num2){
//     var res= num1*num2;
//     return res;


// }

// var result = multiply(2,5);
// console.log('result is >>',result);

// function goToShop(location){
//     var phones =['mi','apple'];
//     var headphones='headphones';
//     var otherItems = 'otheritems';
    
//     // es6 object shorthand
//     return {
//         phones:phones,
//         headphones:headphones,
//         otherItems:otherItems
//     }
//     // RETURN BHANDA TALA CODE EXECUTE HUDAINA IE RETURN STOPS CODE EXECUTION
//     //  so we use array or object


// }


// var result = goToShop('bkt');
// console.log('result is >>', result)


// 5. IIFE
// Immidiately Invoked Functional Expression
// syntax
// ()()

// function test(){
//     console.log('hi i am test');
// }
// // console.log('check test>>', test)
// (test)()

// ()() ekchoti run huncha ani life sakincha yo function ko
// argument pathauna (test)('argument')

(
    function(){
        console.log('i am anynamous function');
        console.log('i am taking help from IIFE to get executed')
    }
)()



// ECMA 
// ECMAscript
// es
// es5 || es2014

// stable js (es5)
// modern js (es6 and beyond)