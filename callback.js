

// function askMoney(amt){

//     console.log('i ask money with mom');
//     console.log('mom told to wait fewdays till she receives salary')
//     // return amt;
//     setTimeout(function(){
//         console.log('mom received salary')
//         return amt;

//     },3000)
// }


// console.log('i want to biuy something')
// var result=askMoney(55555);
// console.log('i have money now', result)
// console.log('now i buy laptop') //blocking tas
// console.log('eat food')//non blocking task


// callback
// callback is a function which is passed as an argument to another function
// argument ma pass hune function lai callback function bhanincha
// callback is a solution to asaync call
// challenges of async call(result handling) can be fullfilled using callbacks


// task

/*
function askMoney(amt,abcd){ //higher order function
    // what comes in check
    console.log('mom told me tow ait till she receives her salary')
    setTimeout(function(){
        console.log('mom receives her salary');
        console.log('now callback');
        abcd();
    }, 2000)
}

console.log('start execution >>')
console.log('i want to buy laptop but i dont have money')
console.log('ask money')
askMoney(33333, function(){ //callback
    // callback function block
    console.log('result of ask money')
    console.log('now perform blocking task')
    console.log('buy laptop')
})

console.log ('perform non blocking task')
console.log('have coddee')
console.log('go to college')

*/


// task 1
// prepare a story to buy phone
// assume there will be delay in shop

// perform some nonblocking task 

// after you have phone give me a call or take photo


function buyPhone(model,price){
    console.log('the model i wish ' +model+  ' will be available after 2 days')

    setTimeout(function(){
        console.log('model received in the shop')
        console.log('now call back')
        console.log('shopkeeper called me')
        console.log('i went to shop')
        price();
    }, 2000)
}

console.log('start execution >>')
console.log('i want to buy a phone')
console.log('i went to tamrakar house')
console.log('i ask shopkeeper the model i want to buy')
buyPhone('12promax', function(){
    console.log('now performing blocking task')
    console.log('buy phone')
})

console.log ('perform non blocking task')
console.log('i went home')
console.log('i play game')
console.log('i slept')
console.log('i woke up and went to college')


