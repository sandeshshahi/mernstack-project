// ##################################### TASK PART #############################################
function askMoney(amt,cb){
    console.log('mom told me to wait')
    setTimeout(function(){
        console.log('mom receives her salary')
        //cb('single argument'); //triggers if block 
        cb(null,amt+10000); //triggers else or khali pathauda ni else block trigger huncha
    },3000)

}

function buyPhone(model,cb){
    console.log('model is not available right now so i should wait')
    setTimeout(function(){
        console.log('model found at shop')
        cb(null, model)
    },1000)
}

function takePhoto(location,cb){
    console.log('choosing loacation to take photo')
    setTimeout(function(){
        console.log('location found and photo taken')
        cb(null,'done');
    },2000)
}

function editPhoto(rawPhoto,cb){
    console.log('editing in progress')
    setTimeout(function(){
        console.log('editing done')
        cb(null,'completed')
    }, 3000)
}

function upLoadPhoto(editedPhoto,cb){
    console.log('upload in progress');
    setTimeout(function(){
        console.log('upload completed');
        cb(null,'done')
    },2000)
}

// #################################### TASK PART #############################################


// *********************************** EXECUTION PART *****************************************

console.log('I WANT TO UPLOAD PHOTO')
console.log('I DONT HAVE PHONE SO GET ONE')
console.log('I DONT HAVE MONEY  SO ASK MONEY')
askMoney(10000,function(err,done){
    console.log('result of ask money >>');
    if(err){
        console.log('error in asking money',err);
    } else {
        console.log('success of asking money',done);
        console.log('now buy phone')
        buyPhone('s10',function(err,done){
            if(err){
                console.log('i dont have phone',err);
            }else{
                console.log('i have phone >>',done)
                console.log('now take photo');
                takePhoto('bkt',function(err,done){
                    if(err){
                        console.log('error in taking photo');
                    }
                    else{
                        console.log('photo is readdy')
                        console.log('now edit photo');
                        editPhoto('adjust contrast',function (err,done){
                            if(err){
                                console.log('error in editing photo')
                            
                            }else{
                                console.log('success editing')
                                upLoadPhoto('sksksks',function(err,done){
                                    if(err){
                                        console.log('error uploading >>',err)
                                    }else{
                                        console.log('upload successful',done)
                                    }
                                })
                            }
                        })
                        
                        console.log('i go a phone call')
                    }
                })
                console.log('its start raining')
            }
        })

        // non blocking task for buy phone
        console.log('have coffee')
    }
    
})

console.log('non blocking code for ask money')
console.log('eat food and sleep')
console.log('go to college')

// buyPhone('12pro',function(err,done){
//     if(err){
//         console.log('i dont have phone')
//     }else{
//         console.log('i have phone')
//     }
// })



// *********************************** EXECUTION PART *****************************************
