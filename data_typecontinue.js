// 2 non primitive datatypes

var name = 'broadway infosys nepal';
var addr = 'tinkune';
const phone = '33333,4444,55555';


// 1. Array 2. Object

//  1.array ==>
// syntax
var fruits = new Array(); // constructor syntax
var vegetables = []; //bracket notation syntax


var languages =['js','php','python','java'];
console.log('languages is>>' , languages)
// items inside array are called elements of array
// array saang sangai aune kura is index of element that start form [0]


var arr = ['skldjf',333,true,null,undefined,['a','b']];
console.log('arr is>>',arr);  

var arr = [['a','b'],'skldjf',333,true,null,undefined];
console.log('arr is>>',arr[0])

var arr = [['a','b'],'skldjf',333,true,null,undefined];
console.log('arr is>>',arr[0][0])  //array bhitra ko array ko element print garna euta tarika
// prints a

// arko tarika
var firstval = arr[0];
console.log('arr is>>',firstval[0]) //this also prints value inside array ie a
// taara yesma suru ma firstval ma 1st ko array lai memory ma halyo tespacchi tyo array ko value lai element wise nikalyo


// array in JS is heterogenous array
// ie JS ko array ma specific data type ko array hunu pardaina array bhitra jasto datatype ko contents or elements ni store garna milcha



// Object datatype
// very important in JS, everything is object
// Object is collection of key-value pair
// property-value pair
// name-value pair

// syntax
var obj = new Object(); //constructor object
var abc = {} //object 

var laptop = {
    name:'inspiron',
    brand:'dell',
    generation:'i7',
    ram:'8gb',
    color:'black',
    tags:['laptop','xps','dell'],
    more_details:{
        a:'b'
    }


}
var ram ='color' //reference for bracket notation line 75

// accessing value of object
// two ways of accessing values
// 1. dot notation
console.log('value is>>', laptop.tags)


// 2. bracket notation ==> recommended to use with references
console.log('bracket notation>>', laptop[ram])
console.log('bracket notation>>', laptop['ram'])

// adding property and value in a object 
laptop.price = 3333;
laptop.tags = 'no any tags';
laptop['name'] ='new name';


// deleting property and value
delete laptop.name;
delete laptop['color']
console.log('laptop now>>',laptop)



// name,brand,generation are key(property or name)
// xps,dell,i5,black... are values
// so key and values are separated by colon
// each pair is separated by comma
