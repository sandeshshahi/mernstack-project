// application data
// const name = 'broadway infosys nepal';
// var address ='tinkune';
// var phone = 1234;
// function hi(){
//     console.log('i am hi');
// }

// scope is accessibility of application data
// types of scope
// 1. global scope
// 2. local scope (functional scope)
// 3. block scope


// 1. global scope
// all those application data which are accessible through out the file are global scope
// generally global scope are kept in outermost layer of a file
 
// 2. local scope (functional scope)
// all those application data declared inside functional block and can be accessible within that functional block
 


// "use strict" // usually top of the file rakhchau yesle function bhitra global variable declare garna didaina ie declare garna var cons let halnu parne huncha 
const name = 'broadway infosys nepal';
// var address ='tinkune';
var phone = 1234;
function hi(){
    console.log('i am hi');
}

function sayHello(){
    // "use strict" // function bhitra use garyo bhane yesko scope function bhitra matra huncha
     address ='bkt' // or function bhitra ni var cons chaina bhane afai global scope huncha or yesma yedi var cons let garena bhane exsiting global ko value replace huncha
    console.log('name inside>>',address)

    var email ='sandesh@gmail.com';
    var hobbies =['coding ','cycling'];
    function getsomething(){
        console.log('name inside another function >>',name)

    }
    console.log('email inside >>',email)
    getsomething();
}

function welcome(){
    var email = '4366@gmail.com';
    function getsomething(){

    }
}

sayHello();
console.log('address outside >>',address)
// console.log ('email outside >>',email)

var laptop = 'dell'

function buylaptop(amount){
    let laptop = 'hp'
    if(amount > 10000){ // euta function bhitra jati choti ni var lekhera declare garda euta matra memory allocate garcha ie single declaration
        let laptop2 = 'macbook' 
        // laptop2 cannot be accessed outside if block 
    } else {
        let laptop3 ='samsung'
        // laptop3 cannot be accessed outside else block
    }

    console.log('laptop now>>',laptop)

}

buylaptop(2000);
console.log('laptop outside >>',laptop);

// var le functional scope maintain garcha bhane let le block scope maintain garcha
