// setTimeout() ==> delay
// setImmediate()
// setInterval()

// console.log('before set immediate')
// setImmediate(function(){
//     console.log('at immidiate')
// })
// console.log('after set immediate')

// // setInterval()
// var i=1;
// var a = setInterval(function(){
//     i++;
//     console.log('in every 2 sec',i)
//     if (i==5){
    
//         clearInterval(a); //used to stop  
//     }
    
// },200)


// typeof ===>  prints type of value

// console.log('type of >>',typeof'sft')
// console.log('type of >>',typeof('sft'))  // function bhako karan le yesari garda ramro
// console.log('type of >>',typeof(333))
// console.log('type of >>',typeof(function(){}))
// console.log('type of >>',typeof({}))
// console.log('type of >>',typeof([]))


// datatype wise inbuilt methods
// string

var str='Braodway Infosis Nepal ';
console.log('length >>',str.length)
console.log('to uppercase >>',str.toUpperCase())
console.log('to lower case >>',str.toLowerCase())
console.log('trim >>',str.trim().length) // to remove white spaces


// substring ===> 1st argument is starting index 
// 2nd argument is ending index(excluded)
console.log('substring >>',str.substring(1,5));


// substr
console.log('substr >>',str.substr(1,5))
// 2nd argument is count


// type conversion
var abc=333;
var intoStr = String(abc);
console.log('check type', typeof(intoStr))

var hobbies='singing,dancing,cycling'
console.log('into array >>',hobbies.split(',')) //returns array

// check existance
var email = 'sandesh@gmail.com';
console.log('check existance',email.includes('.com'))


// number
var num = 3652.2552
console.log('fixed decimal point >>', num.toFixed(2))
console.log('integer value only >>',parseInt(num))
console.log(' parse float >>',parseFloat(num))
console.log(' parse float >>',parseFloat('55.66'))
console.log(' parse float >>',parseFloat('asdasd55as')) //result NaN ( Not a Number)



// coercion (type conversion)
// implicit coercion
// javascript itself convert
// explicit coercion
// developer conversion

function multiply(num1,num2){

    return num1*num2
    
}

var res = multiply('3',4)  //implicit ccoercion ie js le afai string lai number ma convert garyo yedi tyo string value convert huna sakne cha 
var res = multiply('safasd',4)// this results NaN 
console.log('result is >> ',res)


console.log('is number >>',isNaN('555'))

console.log('is finite >>',isFinite('5555'))


var a = '333'
console.log('explicit coercion',Number(a))


// boolean
// boolean(val) ==> return either true or false
// truthy value returns true
// falsy valye return false
// falsy value '',0,NaN,null,undefined,false

// null, undefined

// Object
var laptop={
    name:'xps',
    brand:'dell',
    price: 44,
    generation: 'i7'
}

// console.log('check value',laptop.price)

// check property existance
console.log('check property existance',laptop.hasOwnProperty('price'))

// 




