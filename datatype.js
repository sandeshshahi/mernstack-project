// datatype ==> datatype is classification of data 
//  JS way of classification
// 1.primitive datatype
// 2.non-primitive datatype

// primitive datatype
/*
1.string ==> single quote or double quote enclosed value are always string
eg '333','true',"false",'sandesh'
2.number ==> any numeric value
eg 452,255.2345,23
3.boolean ==> true or flase
4.undefined ==> memory allocated but value not defined
eg
var name;
const addr;
let something;
5.null ==> non existent memorey, can be referred as null
6.symbol ==> maintains uniqueness
7.BigInt ==> highest number 
*/

var address;  //undefined;

// address= 'tinkune' //string
// address= 333; //number
// address = true //boolean

// lesson 1 JS IS LOOSELY TYPED PROGRAMMING LANGUAGE
// WEAKLY TYPED,DYNAMICALLY


console.log('value is >>',address)

